#include <iostream>

using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
    private:
        string serie;
        string empresa;
    
    public:
    	//Constructor
    	Contenedor();
    	
    	//Metodos
        string get_serie();
        string get_empresa();

        void set_serie(string serie);
        void set_empresa(string empresa);
};
#endif
