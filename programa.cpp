#include <iostream>
#include <stdio.h>
#include "Contenedor.h"
#include "Pila.h"

using namespace std;

void op_1(Pila pila[], int cant_pilas){
	int pila_selec;
	string num_serie;
	string nom_empresa;

	cout << "******AGREGAR CONTENEDOR A LA PILA*******" << endl;
	//selecciona la columna que utilizará
	cout << "Ingresa el numero de la pila: ";
	cin >> pila_selec;


	//ingresa la info del contenedor
	cout << "\n\tREGISTRO DE CONTAINER\n" << endl;
	cout << "Ingrese serie del container: ";
	cin >> num_serie; //serie
	cout << "Ingrese empresa encargada del contenedor: ";
	cin >> nom_empresa; //mombre de empresa
	
	Container container = Container();
	container.set_serie(num_serie);
	container.set_empresa(nom_empresa);

	pila[pila_selec].Push(container); 
			
}

void op_2(Pila pila[], int cant_pilas, int limite){
	int pila_selec;
	string num_serie;
	string nom_empresa;

	cout << "******ELIMINAR CONTENEDOR******" << endl;
	cout << "[BUSQUEDA]" << endl;
	cout << "Ingresa el numero de la pila: ";
	cin >> pila_selec;
	cout << "Ingresa serie del contenedor: ";
	cin >> num_serie; //serie
	cout << "Ingresa empresa encargada del container: ";
	cin >> nom_empresa; //empresa

	while(true){
		Container container = pila[pila_selec].Pop(); //entra en la pila
		if (container.get_empresa().compare(nom_empresa))
		{
			/* code */
			if (container.get_serie() == num_serie)
			{
				//si coincide la serie muestra el diagrama
				diagramaPuerto(pila, cant_pilas, limite);
				break;
			}
		}
		else{
			for (int i = 0; i < cant_pilas; ++i)
			{
				
				if (i != pila_selec)
				{
					
					pila[i].Pila_llena();
					if (pila[i].get_band() == false)
					{
						
						pila[i].Push(container);
						diagramaPuerto(pila, cant_pilas, max);
						break;
					}
				}							
			}
		}
	}

}

void diagramaPuerto(Pila pila[], int cant_pilas, int limite){
	cout << "*****DIAGRAMA PUERTO SECO******" << endl;

	for (int i = (limite);i>=0; i--){
    	for(int j=0; j<cant_pilas; j++){
      		pila[j].Show(i);
    	}
	    cout << endl; //separa los niveles (limite)
  	}

  	for(int i=0; i < cant_pilas; i++){
    	cout << "-----------------------"<<endl;
    	cout << "  Columna " << i+1 << "  "<<endl;
    	cout << "-----------------------";
    }

	cout << endl;
}

void menu(Pila pila, int cant_pilas, int limite){
	int opcion;
	
	cout << "**************MENU**************" << endl;
	cout << " [1] Agregar contenedor" << endl;
	cout << " [2] Remover contenedor" << endl;
	cout << " [3] Ver Puerto" << endl;
	cout << " [4] Salir" << endl;
	
	cout << "Ingresa una opción: ";
	cin >> opcion;

	while(getchar() != '\n');

	switch(opcion){
		case 1:
			diagramaPuerto(pila, cant_pilas, limite);
			
			op_1(pila, cant_pilas);
			
			menu(pila, cant_pilas, limite); //volver al menu
			break;

		case 2:
			op_2(pila, cant_pilas, limite);
			menu(pila, cant_pilas, limite);
			break;

		case 3:
			diagramaPuerto(pila, cant_pilas, limite);
			menu(pila, cant_pilas, limite);
			break;

		case 4:
			exit(1);
			break;

		default:
			0cout << "Opcion no valida" << endl;
			break;
	}

}

int main(){
	int cant_pilas; //cantidad de pilas en el puerto
	int limite; //tamaño de las pilas

	

	cout << "*******BIENVENIDO********" << endl;
	cout << "Ingresa la cantidad de pilas: " << endl;
	cin >> cant_pilas;
	//inicia N pilas, N corresponde a la cantidad de pilas
	Pila *pila = new Pila[cant_pilas];
	
	cout << "Ingresa la cantidad maxima de containers por pila: " << endl;
	cin >> limite;

	for (int i = 0; i < cant_pilas; ++i)
	{
		// pasar el largo a las N pilas
		pila[i].Pila_armar(limite);
	}
	
	while(true){
		/* mostrar Menu */
		menu(pila, cant_pilas, limite);
	}
	
	return 0;
}
