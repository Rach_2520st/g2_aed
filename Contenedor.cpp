#include <iostream>
#include "Contenedor.h"

using namespace std;

/*Constructor*/
Contenedor::Contenedor(){
	this->empresa = "DISPONIBLE";
}

/*Metodos*/
string Contenedor::get_serie(){
    return this->serie;
}

string Contenedor::get_empresa(){
    return this->empresa;
}

void Contenedor::set_serie(string serie){
    this->serie = serie;
}

void Contenedor::set_empresa(string empresa){
	this->empresa = empresa;
}