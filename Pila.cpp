#include <iostream>
#include "Contenedor.h"
#include "Pila.h"

using namespace std;

/* constructor */
Pila::Pila(){

}


int Pila::get_limite(){
	return this->limite;
}


bool Pila::get_band(){
  return this->band;
}

void Pila::set_limite(int limite){
	this->limite = limite;
}

void Pila::Pila_armar(int limite){
	this->limite = limite;
	this->arreglo = new Container[this->limite];
}

// boolean para ver estado de la pila
void Pila::Pila_vacia(){
	this->tope = tope;
	if(tope == 0){
		this->band = true; // si es verdadero la pila está vacia
	}
	else{
		this->band = false; //si es falso la  pila está llena 
	}
}

// boolean para ver estado de la pila 
void Pila::Pila_llena(){
	this->tope = tope;
	if(tope == this->limite){
		this->band = true; //pila llena
	}
	else{
		this->band = false; //pila con espacio
	}
	
}

// agregar contenedor
void Pila::Push(Container container){
	
	//ver estado
	Pila_llena();
	this->band = band;
	this->tope = tope;

	if (band == true){
		cout << "[Desbordamiento]" << endl;
		cout << "Pila llena, elimine algun/os elemento/s para agregar" << endl;
	}
	else{
		tope = (tope + 1);
		// Se añade el elemento en el nuevo tope
		this->arreglo[tope] = container;
		cout << "Container ingresado con exito" << endl;
	}
}

//eliminar contenedor 
Container Pila::Pop(){	
	//comprobar el estado
	Pila_vacia();
	this->band = band;
	this->tope = tope;

	Container temporal = Container(); 

	/si está vacio
	if (band == true){
		cout << "[Subdesbordamiento]" << endl;
		cout << "Pila vacı́a, ingrese algun/os elemento/s para eliminar" << endl;
	}
	
	else{
		temporal = this->arreglo[tope];
		this->arreglo[tope].set_serie("0");
		this->arreglo[tope].set_empresa("Disponible");
		tope = (tope - 1);
		cout << "Container extraido: " << temporal << endl;
	}
}

// mostrar contenido de UNA pila
void Pila::Show(int x){
	cout << "\n\tMOSTRAR PILA\n" << endl;
	
	cout << "|" << arreglo[x].get_empresa();
	if (arreglo[x].get_serie() != 0) //no está vacio
	{
		
		cout << "/" << arreglo[x].get_serie();
	}
	else{
		cout << "| "; 
	}
	
			
}
